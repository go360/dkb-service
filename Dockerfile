FROM adoptopenjdk:11-jre-hotspot-bionic
COPY target/dkb-service.jar .
CMD ["java", "-jar", "dkb-service.jar"]
# README #

This README helps to compile, test and run the code 

# Assumptions #
* This is a secure system so I do not need to authenticate or implement HTTPS / VPN based communication
* This is a same bank transaction system (Transfer / Deposit etc) 
* I assumed that in this Code Challenge we are not taking care of ## GDPR ##
* One Customer will have only one account of a Type, such as One Customer can have only one Private account but can have another Saving_Account | Current_Account 

Spring Profiles:
* default : For Local PostGreSQL Storage

-Dspring.profiles.active=[default]

## Project ##
DKB Code Service

### What is this repository for? ###
* Coding challenge

### How do I get set up? ###

* Install Java 11
* Install Maven (Latest)
* Lombok setup please visit [https://projectlombok.org/setup/overview]

### Build, Test and Run ###

* mvn package
* docker-compose up -d
* Test with Swagger on your browser Local [http://localhost:8860/swagger.html] 

### Code Coverage ###
* Coverage: 56.2%
* Covered Instructions: 1,950
* Missed Instructions: 1,620
* Total Instructions: 3,570

### Who do I talk to? ###

* mysialkot@hotmail.com
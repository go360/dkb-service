package com.dkb.account.repository;

import java.util.List;
import java.util.UUID;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;

import com.dkb.account.model.Customer;

public interface CustomerRepository extends PagingAndSortingRepository<Customer, UUID> {

	@Query("From Customer")
	public List<Customer> findAll();
}

package com.dkb.account.repository;

import java.time.LocalDateTime;
import java.util.UUID;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;

import com.dkb.account.model.BankAccount.Type;
import com.dkb.account.model.Transaction;

public interface TransactionRepository extends PagingAndSortingRepository<Transaction, UUID> {

	@Query(value = "SELECT t FROM Transaction t where t.IBAN = :iban and t.createdAt > :after")
	Page<Transaction> findByIban(@Param("iban") String iban, @Param("after") LocalDateTime date, Pageable page);

	
	@Query(value = "SELECT t FROM Transaction t, BankAccount b where t.IBAN = b.IBAN and b.type = :type and t.createdAt > :after")
	Page<Transaction> findByType(@Param("type") Type type, @Param("after") LocalDateTime date, Pageable page);

}

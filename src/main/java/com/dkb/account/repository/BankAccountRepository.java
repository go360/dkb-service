package com.dkb.account.repository;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.transaction.annotation.Transactional;

import com.dkb.account.model.BankAccount;

public interface BankAccountRepository extends PagingAndSortingRepository<BankAccount, String> {

	@Query(value = "FROM BankAccount where customerId = ?1")
	List<BankAccount> findAllByCustomer(UUID customerId);

	@Modifying
	@Query("UPDATE BankAccount SET lock = ?2 WHERE IBAN = ?1")
	@Transactional
	Integer toggleLockBankAccount(String iban, Boolean lock);

	@Modifying
	@Query("UPDATE BankAccount SET balanceAmount = (?2 + (SELECT balanceAmount FROM BankAccount where IBAN = ?1)) WHERE IBAN = ?1")
	@Transactional
	Integer updateBalanceAmount(String iban, Long amount);

	@Query(value = "SELECT lock FROM BankAccount where IBAN = ?1")
	Boolean isLocked(String iban);

	@Query(value = "SELECT balanceAmount FROM BankAccount where IBAN = ?1")
	Long findBalanceAmount(String iban);

	@Query(value = "FROM BankAccount where IBAN = ?1")
	Optional<BankAccount> findByIBAN(String iban);

}

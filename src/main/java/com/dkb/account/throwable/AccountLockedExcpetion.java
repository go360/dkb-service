package com.dkb.account.throwable;

public class AccountLockedExcpetion extends RuntimeException {

	private static final long serialVersionUID = -6231473179084095203L;

	public AccountLockedExcpetion() {
		super();
	}
	
	public AccountLockedExcpetion(String message) {
		super(message);
	}

	public AccountLockedExcpetion(Throwable e) {
		super(e);
	}

	public AccountLockedExcpetion(String message, Throwable e) {
		super(message, e);
	}

}

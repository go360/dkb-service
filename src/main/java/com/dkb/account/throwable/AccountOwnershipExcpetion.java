package com.dkb.account.throwable;

public class AccountOwnershipExcpetion extends RuntimeException {

	private static final long serialVersionUID = -6231473179084095203L;

	public AccountOwnershipExcpetion() {
		super();
	}
	
	public AccountOwnershipExcpetion(String message) {
		super(message);
	}

	public AccountOwnershipExcpetion(Throwable e) {
		super(e);
	}

	public AccountOwnershipExcpetion(String message, Throwable e) {
		super(message, e);
	}

}

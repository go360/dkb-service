package com.dkb.account.throwable;

public class AccountRulesViolationExcpetion extends RuntimeException {

	private static final long serialVersionUID = -6231473179084095203L;
	private ErrorMessage error;

	public AccountRulesViolationExcpetion() {
		super();
	}

	public AccountRulesViolationExcpetion(Integer code, String message) {
		this.error = new ErrorMessage(code, message);
	}
	
	public AccountRulesViolationExcpetion(ErrorMessage error) {
		this.error = error;
	}

	public AccountRulesViolationExcpetion(Throwable e) {
		super(e);
	}

	public AccountRulesViolationExcpetion(String message, Throwable e) {
		super(message, e);
	}

	public ErrorMessage getErrorMessage() {
		return this.error;
	}

}

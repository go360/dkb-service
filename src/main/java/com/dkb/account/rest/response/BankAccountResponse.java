package com.dkb.account.rest.response;

import java.time.LocalDateTime;
import java.util.Collection;
import java.util.UUID;
import java.util.stream.Collectors;

import org.springframework.beans.BeanUtils;

import com.dkb.account.model.BankAccount;
import com.dkb.account.model.BankAccount.Type;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import lombok.Data;

@Data
@JsonInclude(value = Include.NON_NULL)
public class BankAccountResponse {

	private String IBAN;
	private String IBANRef;
	private Type type;
	private Long balanceAmount = 0l;
	private Boolean lock = false;
	private UUID customerId;
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	private LocalDateTime createdAt;
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	private LocalDateTime lastUpdatedAt;

	public static BankAccountResponse from(BankAccount bankAccount) {
		BankAccountResponse response = new BankAccountResponse();
		BeanUtils.copyProperties(bankAccount, response);
		return response;
	}

	public static Collection<BankAccountResponse> from(Collection<BankAccount> bankAccounts) {
		return bankAccounts.stream().map(bc -> from(bc)).collect(Collectors.toList());
	}
}

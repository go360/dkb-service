package com.dkb.account.rest.response;

import java.time.LocalDateTime;
import java.util.Collection;
import java.util.UUID;
import java.util.stream.Collectors;

import org.springframework.beans.BeanUtils;

import com.dkb.account.model.Customer;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import lombok.Data;

@Data
@JsonInclude(value = Include.NON_NULL)
public class CustomerResponse {

	private UUID id;
	private String firstName;
	private String lastName;
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	private LocalDateTime createdAt;
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	private LocalDateTime lastUpdatedAt;

	public static CustomerResponse from(Customer customer) {
		CustomerResponse response = new CustomerResponse();
		BeanUtils.copyProperties(customer, response);
		return response;
	}

	public static Collection<CustomerResponse> from(Collection<Customer> customers) {
		return customers.stream().map(bc -> from(bc)).collect(Collectors.toList());
	}
}

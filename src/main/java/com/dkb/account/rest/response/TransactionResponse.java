package com.dkb.account.rest.response;

import java.time.LocalDateTime;
import java.util.Collection;
import java.util.UUID;
import java.util.stream.Collectors;

import org.springframework.beans.BeanUtils;

import com.dkb.account.model.Transaction;
import com.dkb.account.model.Transaction.Agent;
import com.dkb.account.model.Transaction.TType;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import lombok.Data;

@Data
@JsonInclude(value = Include.NON_NULL)
public class TransactionResponse {

	private UUID id;
	private String IBAN;
	private Agent agent;
	private String agentInfo;
	private Long amount;
	private TType type;

	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	private LocalDateTime createdAt;

	public static TransactionResponse from(Transaction transaction) {
		TransactionResponse response = new TransactionResponse();
		BeanUtils.copyProperties(transaction, response);
		return response;
	}

	public static Collection<TransactionResponse> from(Collection<Transaction> tra) {
		return tra.stream().map(bc -> from(bc)).collect(Collectors.toList());
	}
}

package com.dkb.account.rest.response;

import java.util.Collection;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public class PageResponse<T> {

	private Integer pageSize;
	private Integer pageNumber;
	private Long totalElements;
	private Collection<T> content;
}

package com.dkb.account.rest.controller;

import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import com.dkb.account.throwable.AccountLockedExcpetion;
import com.dkb.account.throwable.AccountRulesViolationExcpetion;
import com.dkb.account.throwable.ErrorMessage;

import lombok.extern.slf4j.Slf4j;

@ControllerAdvice
@Slf4j
public class RestExceptionHandler extends ResponseEntityExceptionHandler {

	@ExceptionHandler(Exception.class)
	public ResponseEntity<?> handle(Exception e) {
		log.error("Error: {}", e);
		return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
	}

	@ExceptionHandler(DataIntegrityViolationException.class)
	public ResponseEntity<?> handle(DataIntegrityViolationException e) {
		log.error("Error: {}", e);
		return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(e.getMessage());
	}

	@ExceptionHandler(AccountLockedExcpetion.class)
	public ResponseEntity<?> handle(AccountLockedExcpetion e) {
		log.error("Error: {}", e);
		return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("IBAN:" + e.getMessage());
	}

	@ExceptionHandler(AccountRulesViolationExcpetion.class)
	public ResponseEntity<ErrorMessage> handle(AccountRulesViolationExcpetion e) {
		log.error("Error: {}", e);
		return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(e.getErrorMessage());
	}
}

package com.dkb.account.rest.controller;

import java.util.UUID;

import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.dkb.account.model.BankAccount.Type;
import com.dkb.account.model.Transaction;
import com.dkb.account.model.Transaction.Agent;
import com.dkb.account.rest.response.BankAccountResponse;
import com.dkb.account.rest.response.PageResponse;
import com.dkb.account.rest.response.TransactionResponse;
import com.dkb.account.service.BankAccountService;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;

@RestController
@RequestMapping(value = "/bank/account")
@Tag(name = "Bank Account")
@RequiredArgsConstructor
public class BankAccountController {

	private final BankAccountService bankAccountService;

	@GetMapping(value = "/{customerId}")
	@Operation(summary = "Return All Bank Accounts of the Customer", description = "List All Bank Accounts of the Customer", tags = "Bank Accounts of The Customer")
	public ResponseEntity<?> accounts(@PathVariable UUID customerId) {
		return ResponseEntity.ok(BankAccountResponse.from(bankAccountService.findBankAccountByCustomerId(customerId)));
	}

	@PostMapping(value = "/register")
	@Operation(summary = "Register new BankAccount", description = "Register a new BankAccount")
	public ResponseEntity<?> account(@RequestParam UUID customerId, @RequestParam String iban,
			@RequestParam Type type) {
		return ResponseEntity.ok(BankAccountResponse.from(bankAccountService.create(customerId, iban, type)));
	}

	@PutMapping(value = "/reference")
	@Operation(summary = "Add Reference Account", description = "Add a reference account")
	public ResponseEntity<?> referenceAccount(@RequestParam String iban, @RequestParam String ibanRef) {
		bankAccountService.referenceAccount(iban, ibanRef);
		return ResponseEntity.status(HttpStatus.NO_CONTENT).build();
	}

	@PutMapping(value = "/lock")
	@Operation(summary = "Toggle Lock BankAccount", description = "Lock / Unlock Bank Account")
	public ResponseEntity<?> lock(@RequestParam String iban, @RequestParam Boolean lock) {
		return ResponseEntity.ok(bankAccountService.toggleLock(iban, lock));
	}

	@PostMapping(value = "/deposit")
	@Operation(summary = "Deposit Money (EUR Cents)", description = "Deposit Money To a Bank Account (IBAN) in EUR Cents")
	@ApiResponses(value = { @ApiResponse(responseCode = "204", description = "Money Has Been Deposit") })
	public ResponseEntity<?> deposit(@RequestParam String iban, @RequestParam Long amount, @RequestParam Agent agent,
			@RequestParam String agentInfo) {
		bankAccountService.deposit(iban, amount, agent, agentInfo);
		return ResponseEntity.status(HttpStatus.NO_CONTENT).build();
	}

	@PostMapping(value = "/transfer")
	@Operation(summary = "Transfer Money (EUR Cents)", description = "Transfer Money Between Bank Accounts in EUR Cents")
	@ApiResponses(value = { @ApiResponse(responseCode = "204", description = "Money Has Been Transfered") })
	public ResponseEntity<?> transfer(@RequestParam String senderIban, @RequestParam String receiverIban,
			@RequestParam Long amount) {
		bankAccountService.transfer(senderIban, receiverIban, amount, Agent.online);
		return ResponseEntity.status(HttpStatus.NO_CONTENT).build();
	}

	@GetMapping(value = "/balance/{iban}")
	@Operation(summary = "Balance of the Bank Account (EUR Cents)", description = "Return the Balance of the Bank Account (EUR Cents)")
	public ResponseEntity<?> balance(@PathVariable String iban) {
		return ResponseEntity.ok(bankAccountService.balance(iban));
	}

	@GetMapping(value = "/transactions/iban/{iban}/{days}/{page_no}")
	@Operation(summary = "List of transactions", description = "Return the transaction history of the given Account_IBAN since {days}")
	public ResponseEntity<?> transactions(@PathVariable String iban, @PathVariable Integer days,
			@PathVariable(name = "page_no") Integer pageNo) {
		final Page<Transaction> page = bankAccountService.transactions(iban, days, pageNo);
		return ResponseEntity.ok(new PageResponse<TransactionResponse>(page.getSize(), page.getNumber(),
				page.getTotalElements(), TransactionResponse.from(page.getContent())));
	}

	@GetMapping(value = "/transactions/type/{type}/{days}/{page_no}")
	@Operation(summary = "List of transactions", description = "Return the transaction history of the given AccountType since {days}")
	public ResponseEntity<?> transactions(@PathVariable Type type, @PathVariable Integer days,
			@PathVariable(name = "page_no") Integer pageNo) {
		final Page<Transaction> page = bankAccountService.transactions(type, days, pageNo);
		return ResponseEntity.ok(new PageResponse<TransactionResponse>(page.getSize(), page.getNumber(),
				page.getTotalElements(), TransactionResponse.from(page.getContent())));
	}

}

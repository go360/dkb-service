package com.dkb.account.rest.controller;

import java.util.Collection;

import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.dkb.account.rest.response.CustomerResponse;
import com.dkb.account.service.CustomerService;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;

@RestController
@RequestMapping(value = "/customers")
@Tag(name = "Customers")
@Validated
@RequiredArgsConstructor
public class CustomerController {

	private final CustomerService customerService;

	@GetMapping(value = "/all")
	@Operation(summary = "Return All Customers", description = "List All customers in the database (Test)", tags = "Customers")
	public ResponseEntity<Collection<CustomerResponse>> customers() {
		return ResponseEntity.ok(CustomerResponse.from(customerService.findAll()));
	}

	@PostMapping(value = "/reg")
	@Operation(summary = "Register a new Customer", description = "Register a new Customer")
	@ApiResponses(value = { @ApiResponse(responseCode = "200", description = "Customer Registered Successfully") })
	public ResponseEntity<CustomerResponse> customer(@RequestParam String firstName, @RequestParam String lastName) {
		return ResponseEntity.ok(CustomerResponse.from(customerService.register(firstName, lastName)));
	}
}

package com.dkb.account.service;

import java.util.List;

import com.dkb.account.model.Customer;

public interface CustomerService {

	Customer register(String firstName, String lastName);

	List<Customer> findAll();

}

package com.dkb.account.service.impl;

import java.util.List;

import org.springframework.stereotype.Service;

import com.dkb.account.model.Customer;
import com.dkb.account.repository.CustomerRepository;
import com.dkb.account.service.CustomerService;

import lombok.RequiredArgsConstructor;

@Service
@RequiredArgsConstructor
public class CustomerServiceImpl implements CustomerService {

	private final CustomerRepository customerRepository;

	@Override
	public Customer register(String firstName, String lastName) {
		Customer customer = new Customer(firstName, lastName);
		return customerRepository.save(customer);
	}

	@Override
	public List<Customer> findAll() {
		return customerRepository.findAll();
	}

}

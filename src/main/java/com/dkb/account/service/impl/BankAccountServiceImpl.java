package com.dkb.account.service.impl;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.UUID;

import javax.persistence.EntityNotFoundException;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.dkb.account.model.BankAccount;
import com.dkb.account.model.BankAccount.Type;
import com.dkb.account.model.Transaction;
import com.dkb.account.model.Transaction.Agent;
import com.dkb.account.model.Transaction.TType;
import com.dkb.account.repository.BankAccountRepository;
import com.dkb.account.repository.TransactionRepository;
import com.dkb.account.service.BankAccountService;
import com.dkb.account.throwable.AccountLockedExcpetion;
import com.dkb.account.throwable.AccountOwnershipExcpetion;
import com.dkb.account.throwable.AccountRulesViolationExcpetion;
import com.dkb.account.throwable.ErrorMessage;

import lombok.RequiredArgsConstructor;

@Service
@RequiredArgsConstructor
public class BankAccountServiceImpl implements BankAccountService {

	private final BankAccountRepository bankAccountRepository;
	private final TransactionRepository transactionRepository;

	@Override
	public BankAccount create(UUID customerId, String iban, Type type) {
		return bankAccountRepository.save(new BankAccount(customerId, iban.replaceAll(" ", ""), type));
	}

	@Override
	public void referenceAccount(String iban, String ibanRef) {

		final Optional<BankAccount> savingOptional = bankAccountRepository.findByIBAN(iban);
		final Optional<BankAccount> checkOptional = bankAccountRepository.findByIBAN(ibanRef);

		if (savingOptional.isEmpty() || checkOptional.isEmpty()) {
			throw new EntityNotFoundException();
		}

		BankAccount savingAccount = savingOptional.get();
		BankAccount checkingAccount = checkOptional.get();

		if (savingAccount.getLock()) {
			throw new AccountLockedExcpetion(iban);
		} else if (checkingAccount.getLock()) {
			throw new AccountLockedExcpetion(ibanRef);
		} else if (!savingAccount.getCustomerId().equals(checkingAccount.getCustomerId())) {
			throw new AccountOwnershipExcpetion();
		}

		if (savingAccount.getType() == Type.SAVINGS_ACCOUNT && checkingAccount.getType() == Type.CHECKING_ACCOUNT) {
			savingAccount.setIBANRef(ibanRef);
			bankAccountRepository.save(savingAccount);
		} else {
			throw new AccountRulesViolationExcpetion(ErrorMessage.SAVING_REF_ERROR);
		}

	}

	@Override
	public Integer toggleLock(String iban, Boolean lock) {
		return bankAccountRepository.toggleLockBankAccount(iban, lock);
	}

	@Override
	public List<BankAccount> findBankAccountByCustomerId(UUID customerId) {
		return bankAccountRepository.findAllByCustomer(customerId);
	}

	@Override
	@Transactional
	public void deposit(String iban, Long amount, Agent agent, String agentInfo) {
		if (bankAccountRepository.isLocked(iban)) {
			throw new AccountLockedExcpetion(iban);
		}

		transactionRepository.save(new Transaction(iban, amount, agent, agentInfo, TType.deposit));
		bankAccountRepository.updateBalanceAmount(iban, amount);

	}

	@Override
	@Transactional
	public void transfer(final String senderIban, final String receiverIban, final Long amount, final Agent agent) {

		final Optional<BankAccount> senderOptional = bankAccountRepository.findByIBAN(senderIban);
		final Optional<BankAccount> receiverOptional = bankAccountRepository.findByIBAN(receiverIban);

		if (senderOptional.isEmpty() || receiverOptional.isEmpty()) {
			throw new EntityNotFoundException();
		}

		final BankAccount sender = senderOptional.get();
		final BankAccount receiver = receiverOptional.get();

		if (sender.getLock()) {
			throw new AccountLockedExcpetion(senderIban);
		} else if (receiver.getLock()) {
			throw new AccountLockedExcpetion(receiverIban);
		}

		if (sender.getBalanceAmount() - amount < 0)
			throw new AccountRulesViolationExcpetion(ErrorMessage.NOT_ENOUGH_BALANCE);
		else if (sender.getType() == Type.PRIVATE_LOAN_ACCOUNT)
			throw new AccountRulesViolationExcpetion(ErrorMessage.PRIVATE_ACC_WITHDRAW);
		else if (sender.getType() == Type.SAVINGS_ACCOUNT && !Objects.equals(sender.getIBANRef(),receiver.getIBAN()))
			throw new AccountRulesViolationExcpetion(ErrorMessage.SAVING_ACC_WITHDRAW);

		transactionRepository.save(new Transaction(senderIban, amount, agent, "ONLINE SELF", TType.transfer));
		bankAccountRepository.updateBalanceAmount(senderIban, -amount);

		transactionRepository.save(new Transaction(receiverIban, amount, agent, senderIban, TType.deposit));
		bankAccountRepository.updateBalanceAmount(receiverIban, amount);

	}

	@Override
	public Long balance(String iban) {
		return bankAccountRepository.findBalanceAmount(iban);
	}

	@Override
	public Page<Transaction> transactions(String iban, Integer days, Integer pageNo) {
		return transactionRepository.findByIban(iban, LocalDateTime.now().minusDays(days),
				PageRequest.of(pageNo, PAGE_SIZE));
	}

	@Override
	public Page<Transaction> transactions(Type type, Integer days, Integer pageNo) {
		return transactionRepository.findByType(type, LocalDateTime.now().minusDays(days),
				PageRequest.of(pageNo, PAGE_SIZE));
	}

}

package com.dkb.account.service;

import java.util.List;
import java.util.UUID;

import org.springframework.data.domain.Page;

import com.dkb.account.model.BankAccount;
import com.dkb.account.model.BankAccount.Type;
import com.dkb.account.model.Transaction;
import com.dkb.account.model.Transaction.Agent;

public interface BankAccountService {

	Integer PAGE_SIZE = 50;

	BankAccount create(UUID customerId, String iban, Type type);

	List<BankAccount> findBankAccountByCustomerId(UUID customerId);

	Integer toggleLock(String iban, Boolean lock);

	void deposit(String iban, Long amount, Agent agent, String agentInfo);

	void transfer(String senderIban, String receiverIban, Long amount, Agent online);

	Long balance(String iban);

	Page<Transaction> transactions(String iban, Integer days, Integer pageNo);

	Page<Transaction> transactions(Type type, Integer days, Integer pageNo);

	void referenceAccount(String iban, String ibanRef);


}

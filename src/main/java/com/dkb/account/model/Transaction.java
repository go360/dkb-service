package com.dkb.account.model;

import java.time.LocalDateTime;
import java.util.UUID;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@Entity
@Table(name = "transaction")
@NoArgsConstructor
public class Transaction {

	@Id
	@GenericGenerator(name = "uuid", strategy = "uuid2")
	@GeneratedValue(generator = "uuid")
	@Column(name = "id", nullable = false)
	private UUID id;

	@Column(name = "iban", nullable = false)
	private String IBAN;

	@Column(name = "agent", nullable = false)
	@Enumerated(EnumType.STRING)
	private Agent agent;

	@Column(name = "agent_info", nullable = false)
	private String agentInfo;

	// Amount saved as EURO 'Cents'
	@Column(name = "amount", nullable = false)
	private Long amount;

	@Enumerated(EnumType.STRING)
	@Column(name = "type", nullable = false)
	private TType type;

	@Column(name = "created_at", nullable = false, updatable = false)
	private LocalDateTime createdAt = LocalDateTime.now();

	public Transaction(String IBAN, Long amount, Agent agent, String agentInfo, TType type) {
		super();
		this.IBAN = IBAN;
		this.agent = agent;
		this.agentInfo = agentInfo;
		this.amount = amount;
		this.type = type;
	}

	public static enum TType {
		withdraw, transfer, deposit
	}

	public static enum Agent {
		atm, bank, online
	}

}

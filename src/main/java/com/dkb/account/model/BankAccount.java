package com.dkb.account.model;

import java.time.LocalDateTime;
import java.util.UUID;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@Entity
@Table(name = "bank_account", indexes = { @Index(columnList = "customer_id, type", unique = true),
		@Index(columnList = "iban", unique = true) })
@NoArgsConstructor
public class BankAccount {

	@Id
	@GenericGenerator(name = "uuid", strategy = "uuid2")
	@GeneratedValue(generator = "uuid")
	@Column(name = "id", nullable = false)
	private UUID id;

	@Column(name = "iban", nullable = false, unique = true)
	private String IBAN;

	@Column(name = "iban_ref")
	private String IBANRef;

	@Enumerated(EnumType.STRING)
	@Column(name = "type", nullable = false)
	private Type type;

	@Column(name = "customer_id", nullable = false)
	private UUID customerId;

	@Column(name = "created_at", nullable = false, updatable = false)
	private LocalDateTime createdAt;

	@Column(name = "last_updated_at", nullable = true, updatable = false)
	private LocalDateTime lastUpdatedAt = LocalDateTime.now();

	// Amount saved as EURO 'Cents'
	@Column(name = "balance_amount", nullable = false)
	private Long balanceAmount;

	@Column(name = "lock", nullable = false)
	private Boolean lock = false;

	public BankAccount(UUID customerId, String iban, Type type) {
		this.customerId = customerId;
		this.IBAN = iban;
		this.type = type;
		this.balanceAmount = 0l;
		this.createdAt = LocalDateTime.now();
	}

	public BankAccount(String iban) {
		this.IBAN = iban;
	}

	public static enum Type {
		CHECKING_ACCOUNT, SAVINGS_ACCOUNT, PRIVATE_LOAN_ACCOUNT
	}

}

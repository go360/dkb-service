package com.dkb.service;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertThrows;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.BDDMockito.given;
import static org.mockito.BDDMockito.then;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.testcontainers.shaded.com.google.common.io.Resources;

import com.dkb.account.model.BankAccount;
import com.dkb.account.model.BankAccount.Type;
import com.dkb.account.model.Transaction;
import com.dkb.account.model.Transaction.Agent;
import com.dkb.account.repository.BankAccountRepository;
import com.dkb.account.repository.TransactionRepository;
import com.dkb.account.service.impl.BankAccountServiceImpl;
import com.dkb.account.throwable.AccountRulesViolationExcpetion;
import com.dkb.account.throwable.ErrorMessage;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;

import lombok.SneakyThrows;

@RunWith(MockitoJUnitRunner.class)
public class BankAccountServiceTest {

	@Mock
	BankAccountRepository accountRepository;

	@Mock
	TransactionRepository transactionrRepository;

	@InjectMocks
	BankAccountServiceImpl service;

	private ObjectMapper mapper = new ObjectMapper();

	@Before
	public void setup() {
		mapper.registerModule(new JavaTimeModule());
		mapper.disable(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS);
	}

	@Test
	@SneakyThrows
	public void saveAccountT() {

		// given
		BankAccount account = mapper.readValue(Resources.getResource("account.json"), BankAccount.class);
		given(accountRepository.save(any())).willReturn(account);

		// when
		BankAccount account_saved = service.create(UUID.randomUUID(), "DE89370400440532013100", Type.CHECKING_ACCOUNT);

		// then
		then(accountRepository).should().save(any());

		assertNotNull(account_saved.getId());
	}

	@Test
	@SneakyThrows
	public void listAccountsT() {

		// given
		UUID customerId = UUID.fromString("0e678940-93d5-4699-a987-5d8c215cdf5c");
		BankAccount[] bankAccounts = mapper.readValue(Resources.getResource("accounts.json"), BankAccount[].class);
		given(accountRepository.findAllByCustomer(customerId)).willReturn(Arrays.asList(bankAccounts));

		// when
		List<BankAccount> list = service.findBankAccountByCustomerId(customerId);

		// then
		then(accountRepository).should().findAllByCustomer(customerId);

		assertEquals(list.size(), 5);
	}

	@Test
	@SneakyThrows
	public void toggleLockBankAccountT() {

		// given
		String IBAN = "DE89370400440532013100";
		given(accountRepository.toggleLockBankAccount(IBAN, true)).willReturn(1);

		// when
		Integer value = service.toggleLock(IBAN, true);

		// then
		then(accountRepository).should().toggleLockBankAccount(IBAN, true);

		assertEquals(value, 1);
	}

	@Test
	@SneakyThrows
	public void findBalanceAmountT() {

		// given
		String IBAN = "DE89370400440532013100";
		given(accountRepository.findBalanceAmount(IBAN)).willReturn(300l);

		// when
		Long balance = service.balance(IBAN);

		// then
		then(accountRepository).should().findBalanceAmount(IBAN);

		assertEquals(balance, 300l);
	}

	@Test
	@SneakyThrows
	public void depositT() {

		// given
		String IBAN = "DE89370400440532013100";
		given(accountRepository.updateBalanceAmount(IBAN, 200l)).willReturn(1);
		given(accountRepository.updateBalanceAmount(IBAN, 200l)).willReturn(1);

		// when
		service.deposit(IBAN, 200l, Agent.online, "ONLINE");

		// then
		then(transactionrRepository).should().save(any(Transaction.class));
		then(accountRepository).should().updateBalanceAmount(IBAN, 200l);

	}

	@Test
	@SneakyThrows
	public void referenceAccountHappyT() {

		// given
		String IBAN = "DE89370400440532013200";
		String IBAN_REF = "DE89370400440532013100";
		BankAccount[] bankAccounts = mapper.readValue(Resources.getResource("accounts.json"), BankAccount[].class);

		// A Checking Account
		given(accountRepository.findByIBAN(IBAN_REF)).willReturn(Optional.of(bankAccounts[0]));
		// A Saving Account
		given(accountRepository.findByIBAN(IBAN)).willReturn(Optional.of(bankAccounts[2]));

		// when
		service.referenceAccount(IBAN, IBAN_REF);

		// then
		then(accountRepository).should().save(any(BankAccount.class));

	}

	@Test
	@SneakyThrows
	public void referenceAccountSadT() {

		// given
		String IBAN = "DE89370400440532013200";
		String IBAN_REF = "DE89370400440532013100";
		BankAccount[] bankAccounts = mapper.readValue(Resources.getResource("accounts.json"), BankAccount[].class);

		// Not A Checking Account
		given(accountRepository.findByIBAN(IBAN_REF)).willReturn(Optional.of(bankAccounts[1]));
		// A Saving Account
		given(accountRepository.findByIBAN(IBAN)).willReturn(Optional.of(bankAccounts[2]));

		// when
		assertThrows(AccountRulesViolationExcpetion.class, () -> {
			service.referenceAccount(IBAN, IBAN_REF);
		});

	}

	@Test
	@SneakyThrows
	public void transferHappyT() {

		// given
		String IBAN_SAVING = "DE89370400440532013200";
		String IBAN_CHECKING = "DE89370400440532013100";
		BankAccount[] bankAccounts = mapper.readValue(Resources.getResource("accounts.json"), BankAccount[].class);

		// A Checking Account
		given(accountRepository.findByIBAN(IBAN_CHECKING)).willReturn(Optional.of(bankAccounts[0]));
		// A Saving Account
		given(accountRepository.findByIBAN(IBAN_SAVING)).willReturn(Optional.of(bankAccounts[2]));

		// when
		service.transfer(IBAN_CHECKING, IBAN_SAVING, 100l, Agent.online);

		// then
		then(accountRepository).should().updateBalanceAmount(IBAN_SAVING, 100l);

	}

	@Test
	@SneakyThrows
	public void transferPrivateAccWithdrawSadT() {

		// given
		String IBAN_SAVING = "DE89370400440532013200";
		String IBAN_PRIVATE = "DE89370400440532013300";
		BankAccount[] bankAccounts = mapper.readValue(Resources.getResource("accounts.json"), BankAccount[].class);

		// A Private Account
		given(accountRepository.findByIBAN(IBAN_PRIVATE)).willReturn(Optional.of(bankAccounts[1]));
		// A Saving Account
		given(accountRepository.findByIBAN(IBAN_SAVING)).willReturn(Optional.of(bankAccounts[2]));

		// when

		// when
		ErrorMessage errorMessage = null;
		try {
			service.transfer(IBAN_PRIVATE, IBAN_SAVING, 100l, Agent.online);
		} catch (AccountRulesViolationExcpetion e) {
			errorMessage = e.getErrorMessage();
		}

		// Then
		assertEquals(errorMessage, ErrorMessage.PRIVATE_ACC_WITHDRAW);

	}

	@Test
	@SneakyThrows
	public void transferSavingExternalTransferSadT() {

		// given
		String IBAN_SAVING = "DE89370400440532013200";
		String IBAN_EXTERNAL = "DE89370400440532013300";
		BankAccount[] bankAccounts = mapper.readValue(Resources.getResource("accounts.json"), BankAccount[].class);

		// A Private Account
		given(accountRepository.findByIBAN(IBAN_SAVING)).willReturn(Optional.of(bankAccounts[2]));
		// A Saving Account
		given(accountRepository.findByIBAN(IBAN_EXTERNAL)).willReturn(Optional.of(bankAccounts[4]));

		// when

		// when
		ErrorMessage errorMessage = null;
		try {
			service.transfer(IBAN_SAVING, IBAN_EXTERNAL, 100l, Agent.online);
		} catch (AccountRulesViolationExcpetion e) {
			errorMessage = e.getErrorMessage();
		}

		// Then
		assertEquals(errorMessage, ErrorMessage.SAVING_ACC_WITHDRAW);

	}

	@Test
	@SneakyThrows
	public void transferLowBalanceSadT() {

		// given
		String IBAN_SAVING = "DE89370400440532013200";
		String IBAN_CHECKING = "DE89370400440532013100";
		BankAccount[] bankAccounts = mapper.readValue(Resources.getResource("accounts.json"), BankAccount[].class);

		// A Private Account
		given(accountRepository.findByIBAN(IBAN_CHECKING)).willReturn(Optional.of(bankAccounts[1]));
		// A Saving Account
		given(accountRepository.findByIBAN(IBAN_SAVING)).willReturn(Optional.of(bankAccounts[3]));

		// when
		ErrorMessage errorMessage = null;
		try {
			service.transfer(IBAN_SAVING, IBAN_CHECKING, 50L, Agent.online);
		} catch (AccountRulesViolationExcpetion e) {
			errorMessage = e.getErrorMessage();
		}

		// Then
		assertEquals(errorMessage, ErrorMessage.NOT_ENOUGH_BALANCE);

	}

}

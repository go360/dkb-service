package com.dkb.service;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.BDDMockito.given;
import static org.mockito.BDDMockito.then;

import java.util.Arrays;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.testcontainers.shaded.com.google.common.io.Resources;

import com.dkb.account.model.Customer;
import com.dkb.account.repository.CustomerRepository;
import com.dkb.account.service.impl.CustomerServiceImpl;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;

import lombok.SneakyThrows;

@RunWith(MockitoJUnitRunner.class)
public class CustomerServiceTest {

	@Mock
	CustomerRepository repository;

	@InjectMocks
	CustomerServiceImpl service;

	private ObjectMapper mapper = new ObjectMapper();

	@Before
	public void setup() {
		mapper.registerModule(new JavaTimeModule());
		mapper.disable(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS);
	}

	@Test
	@SneakyThrows
	public void saveCustomer() {

		// given
		Customer customer = mapper.readValue(Resources.getResource("customer.json"), Customer.class);
		given(repository.save(any())).willReturn(customer);

		// when
		service.register("Sikandar", "Ali");

		// then
		then(repository).should().save(any());

	}

	@Test
	@SneakyThrows
	public void listCustomers() {

		// given
		Customer[] customers = mapper.readValue(Resources.getResource("customers.json"), Customer[].class);
		given(repository.findAll()).willReturn(Arrays.asList(customers));

		// when
		List<Customer> list = service.findAll();

		// then
		then(repository).should().findAll();

		assertEquals(list.size(), 2);
	}

}

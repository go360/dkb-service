package com.dkb.controller;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.UUID;

import org.junit.Ignore;
import org.junit.Test;

import com.dkb.account.model.Transaction.Agent;
import com.dkb.account.rest.response.CustomerResponse;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;

import io.restassured.RestAssured;
import io.restassured.response.Response;
import lombok.SneakyThrows;

public class BankAccountControllerIT {

	static String IBAN = null;;
	static UUID customerId = null;
	static ObjectMapper mapper = new ObjectMapper();
	{
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		mapper.registerModule(new JavaTimeModule());
		mapper.setDateFormat(dateFormat);
		IBAN = String.valueOf(new Date().getTime());
		RestAssured.baseURI = "http://localhost:8860";
	}

	@Test
	@Ignore
	@SneakyThrows
	public void user_journey() {

		Response response = RestAssured.given().param("firstName", "Ali").param("lastName", "Awan")
				.post("/customers/reg").andReturn();

		response.then().statusCode(200);

		response = RestAssured.given().get("/customers/all").thenReturn();
		CustomerResponse[] customers = mapper.readValue(response.getBody().asString(), CustomerResponse[].class);
		response.then().statusCode(200);

		customerId = customers[customers.length - 1].getId();

		response = RestAssured.given().param("iban", IBAN).param("customerId", customerId)
				.param("type", "SAVINGS_ACCOUNT").post("/bank/account/register").andReturn();
		response.then().statusCode(200);

		response = RestAssured.given().param("iban", IBAN).param("amount", 600).param("agent", Agent.online)
				.param("agentInfo", "Nothing").post("/bank/account/deposit").andReturn();

		response.then().statusCode(204);
	}

}

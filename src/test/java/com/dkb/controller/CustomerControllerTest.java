package com.dkb.controller;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.BDDMockito.given;
import static org.mockito.BDDMockito.then;

import java.util.Arrays;
import java.util.Collection;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.http.ResponseEntity;
import org.testcontainers.shaded.com.google.common.io.Resources;

import com.dkb.account.model.Customer;
import com.dkb.account.repository.CustomerRepository;
import com.dkb.account.rest.controller.CustomerController;
import com.dkb.account.rest.response.CustomerResponse;
import com.dkb.account.service.CustomerService;
import com.dkb.account.service.impl.CustomerServiceImpl;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;

import lombok.SneakyThrows;

@RunWith(MockitoJUnitRunner.class)
public class CustomerControllerTest {

	@Mock
	CustomerRepository repository;

	CustomerService service;
	CustomerController controller;

	private ObjectMapper mapper = new ObjectMapper();

	@Before
	public void setup() {
		mapper.registerModule(new JavaTimeModule());
		mapper.disable(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS);

		service = new CustomerServiceImpl(repository);
		controller = new CustomerController(service);
	}

	@Test
	@SneakyThrows
	public void saveCustomer() {

		// given
		Customer customer = mapper.readValue(Resources.getResource("customer.json"), Customer.class);
		given(repository.save(any())).willReturn(customer);

		// when
		ResponseEntity<CustomerResponse> response = controller.customer("Sikandar", "Ali");

		// then
		then(repository).should().save(any());

		assertEquals(customer.getId(), response.getBody().getId());

	}

	@Test
	@SneakyThrows
	public void listCustomers() {

		// given
		Customer[] customers = mapper.readValue(Resources.getResource("customers.json"), Customer[].class);
		given(repository.findAll()).willReturn(Arrays.asList(customers));

		// when
		ResponseEntity<Collection<CustomerResponse>> response = controller.customers();

		// then
		then(repository).should().findAll();

		assertEquals(customers.length, response.getBody().size());
	}

}

package com.dkb.controller;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.BDDMockito.given;
import static org.mockito.BDDMockito.then;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

import org.junit.Before;
import org.junit.Test;
import org.junit.jupiter.api.AfterAll;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.testcontainers.shaded.com.google.common.io.Resources;

import com.dkb.account.model.BankAccount;
import com.dkb.account.model.BankAccount.Type;
import com.dkb.account.model.Transaction;
import com.dkb.account.model.Transaction.Agent;
import com.dkb.account.repository.BankAccountRepository;
import com.dkb.account.repository.TransactionRepository;
import com.dkb.account.rest.controller.BankAccountController;
import com.dkb.account.rest.response.BankAccountResponse;
import com.dkb.account.service.BankAccountService;
import com.dkb.account.service.impl.BankAccountServiceImpl;
import com.dkb.account.throwable.AccountRulesViolationExcpetion;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;

import lombok.SneakyThrows;

@RunWith(MockitoJUnitRunner.class)
public class BankAccountControllerTest {

	@Mock
	BankAccountRepository accountRepository;

	@Mock
	TransactionRepository transactionrepository;

	BankAccountService service;
	BankAccountController controller;

	private ObjectMapper mapper = new ObjectMapper();
	BankAccount[] bankAccounts = null;

	@Before
	@SneakyThrows
	public void setup() {
		mapper.registerModule(new JavaTimeModule());
		mapper.disable(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS);
		service = new BankAccountServiceImpl(accountRepository, transactionrepository);
		controller = new BankAccountController(service);
		bankAccounts = mapper.readValue(Resources.getResource("accounts.json"), BankAccount[].class);
	}

	@AfterAll
	public void tearDown() {
	}

	@Test
	@SneakyThrows
	public void saveAccountT() {

		// given
		UUID customerId = UUID.fromString("0e678940-93d5-4699-a987-5d8c215cdf5c");
		String IBAN = "DE89370400440532013100";
		BankAccount account = mapper.readValue(Resources.getResource("account.json"), BankAccount.class);
		given(accountRepository.save(any())).willReturn(account);

		// when
		ResponseEntity<?> response = controller.account(customerId, IBAN, Type.CHECKING_ACCOUNT);

		// then
		then(accountRepository).should().save(any());

		assertEquals(((BankAccountResponse) response.getBody()).getIBAN(), IBAN);
	}

	@SuppressWarnings("unchecked")
	@Test
	public void listAccountsT() {

		// given
		UUID customerId = UUID.fromString("0e678940-93d5-4699-a987-5d8c215cdf5c");
		given(accountRepository.findAllByCustomer(customerId)).willReturn(Arrays.asList(bankAccounts));

		// when
		ResponseEntity<?> response = controller.accounts(customerId);

		// then
		then(accountRepository).should().findAllByCustomer(customerId);

		assertEquals(((List<BankAccount>) response.getBody()).size(), bankAccounts.length);
	}

	@Test
	public void toggleLockBankAccountT() {

		// given
		String IBAN = "DE89370400440532013100";
		given(accountRepository.toggleLockBankAccount(IBAN, true)).willReturn(1);

		// when
		ResponseEntity<?> response = controller.lock(IBAN, true);

		// then
		then(accountRepository).should().toggleLockBankAccount(IBAN, true);

		assertEquals(response.getStatusCode(), HttpStatus.OK);
	}

	@Test
	public void findBalanceAmountT() {

		// given
		String IBAN = "DE89370400440532013100";
		given(accountRepository.findBalanceAmount(IBAN)).willReturn(300l);

		// when
		ResponseEntity<?> response = controller.balance(IBAN);

		// then
		then(accountRepository).should().findBalanceAmount(IBAN);

		assertEquals(response.getBody(), 300l);
	}

	@Test
	public void depositT() {

		// given
		String IBAN = "DE89370400440532013100";
		given(accountRepository.updateBalanceAmount(IBAN, 200l)).willReturn(1);
		given(accountRepository.updateBalanceAmount(IBAN, 200l)).willReturn(1);

		// when
		ResponseEntity<?> response = controller.deposit(IBAN, 200l, Agent.online, "ONLINE");

		// then
		then(transactionrepository).should().save(any(Transaction.class));
		then(accountRepository).should().updateBalanceAmount(IBAN, 200l);

		assertEquals(response.getStatusCode(), HttpStatus.NO_CONTENT);

	}

	@Test
	public void referenceAccountHappyT() {

		// given
		String IBAN = "DE89370400440532013200";
		String IBAN_REF = "DE89370400440532013100";

		// A Checking Account
		given(accountRepository.findByIBAN(IBAN_REF)).willReturn(Optional.of(bankAccounts[0]));
		// A Saving Account
		given(accountRepository.findByIBAN(IBAN)).willReturn(Optional.of(bankAccounts[2]));

		// when
		ResponseEntity<?> response = controller.referenceAccount(IBAN, IBAN_REF);

		// then
		then(accountRepository).should().save(any(BankAccount.class));

		assertEquals(response.getStatusCode(), HttpStatus.NO_CONTENT);

	}

	@Test
	public void transferHappyT() {

		// given
		String IBAN_SAVING = "DE89370400440532013200";
		String IBAN_CHECKING = "DE89370400440532013100";

		// A Checking Account
		given(accountRepository.findByIBAN(IBAN_CHECKING)).willReturn(Optional.of(bankAccounts[0]));
		// A Saving Account
		given(accountRepository.findByIBAN(IBAN_SAVING)).willReturn(Optional.of(bankAccounts[2]));

		// when
		ResponseEntity<?> response = controller.transfer(IBAN_CHECKING, IBAN_SAVING, 100l);

		// then
		then(accountRepository).should().updateBalanceAmount(IBAN_SAVING, 100l);
		assertEquals(response.getStatusCode(), HttpStatus.NO_CONTENT);

	}

	@Test(expected = AccountRulesViolationExcpetion.class)
	public void referenceAccountSadT() {

		// given
		String IBAN = "DE89370400440532013200";
		String IBAN_REF = "DE89370400440532013100";

		// Not A Checking Account
		given(accountRepository.findByIBAN(IBAN_REF)).willReturn(Optional.of(bankAccounts[1]));
		// A Saving Account
		given(accountRepository.findByIBAN(IBAN)).willReturn(Optional.of(bankAccounts[2]));

		// when
		controller.referenceAccount(IBAN, IBAN_REF);

	}

	@Test(expected = AccountRulesViolationExcpetion.class)
	public void transferPrivateAccWithdrawSadT() {

		// given
		String IBAN_SAVING = "DE89370400440532013200";
		String IBAN_PRIVATE = "DE89370400440532013300";

		// A Private Account
		given(accountRepository.findByIBAN(IBAN_PRIVATE)).willReturn(Optional.of(bankAccounts[1]));
		// A Saving Account
		given(accountRepository.findByIBAN(IBAN_SAVING)).willReturn(Optional.of(bankAccounts[2]));

		// when
		service.transfer(IBAN_PRIVATE, IBAN_SAVING, 100l, Agent.online);

	}

	@Test(expected = AccountRulesViolationExcpetion.class)
	public void transferSavingExternalTransferSadT() {

		// given
		String IBAN_SAVING = "DE89370400440532013200";
		String IBAN_EXTERNAL = "DE89370400440532013300";

		// A Private Account
		given(accountRepository.findByIBAN(IBAN_SAVING)).willReturn(Optional.of(bankAccounts[2]));
		// A Saving Account
		given(accountRepository.findByIBAN(IBAN_EXTERNAL)).willReturn(Optional.of(bankAccounts[4]));

		service.transfer(IBAN_SAVING, IBAN_EXTERNAL, 100l, Agent.online);

	}

	@Test(expected = AccountRulesViolationExcpetion.class)
	public void transferLowBalanceSadT() {

		// given
		String IBAN_SAVING = "DE89370400440532013200";
		String IBAN_CHECKING = "DE89370400440532013100";

		// A Private Account
		given(accountRepository.findByIBAN(IBAN_CHECKING)).willReturn(Optional.of(bankAccounts[1]));
		// A Saving Account
		given(accountRepository.findByIBAN(IBAN_SAVING)).willReturn(Optional.of(bankAccounts[3]));

		service.transfer(IBAN_SAVING, IBAN_CHECKING, 50L, Agent.online);

	}

	

}
